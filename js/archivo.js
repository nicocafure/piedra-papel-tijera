var vNombreGlobal;
var opcionRivalSeleccionada = 0;
var ronda = 1;
var contadorJugador1 = 0;
var contadorJugador2 = 0;

$(document).ready(function () {
  $("#validarNombre").validate({
    rules: {
      nombre: {
        required: true,
        minlength: 3,
      },

      email: {
        required: true,
        email: true,
      },
    },

    messages: {
      nombre: {
        minlength: "Debe incluir al menos 3 caracteres",
        required: "El campo nombre es requerido",
      },

      email: {
        email: "El mail debe tener el formato adecuado ejemplo@dominio.com",
        required: "El campo email es requerido",
      },
    },
  });
});

function ingresar() {
  var vNombre = document.getElementById("nombre").value;
  var campoEmail = document.getElementById("email").value;
  var vValidar = new RegExp(/^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/);
  if (vNombre.trim() != "" || campoEmail.trim() != "") {
    if (vNombre.length >= 3) {
      if (vValidar.test(document.getElementById("email").value)) {
        document.getElementById("seleccionador").style.display = "block";
        document.getElementById("jugadores").style.display = "flex";
        document.getElementById("marcadores").style.display = "flex";
        document.getElementById("resultados").style.display = "block";
        document.getElementById("camposFormulario").style.display = "none";

        vNombreGlobal = vNombre;
        document.getElementById("nombreJugador").innerHTML = vNombreGlobal;

        Swal.fire({
          icon: "success",
          title: "Datos ingresados correctamente",
          text: "Disfruta del Juego!",
        });
      } else {
        Swal.fire({
          icon: "error",
          title: "Ingreso no valido",
          text: "Completa los campos correctamente",
        });
      }
    } else {
      Swal.fire({
        icon: "error",
        title: "Ingreso no valido",
        text: "Completa los campos correctamente",
      });
    }
  } else {
    Swal.fire({
      icon: "error",
      title: "Ingreso no valido",
      text: "Completa los campos vacios",
    });
  }
}

function reset() {
  document.getElementById("imagenjugador1").src = "images/none.gif";
  document.getElementById("imagenjugador2").src = "images/none.gif";
  document.getElementById("botonreset").style.display = "none";
  document.getElementById("botonjugar").style.display = "flex";
  document.getElementById("opciones").style.display = "flex";
  actualizarInfo(``);
}

function actualizarInfo(vInfo) {
  document.getElementById("resultados").innerHTML = vInfo;
}

function nuevoJuego() {
  document.getElementById("imagenjugador1").src = "images/none.gif";
  document.getElementById("imagenjugador2").src = "images/none.gif";
  document.getElementById("botonNuevoJuego").style.display = "none";
  document.getElementById("botonjugar").style.display = "flex";
  document.getElementById("seleccionador").style.display = "block";
  document.getElementById("opciones").style.display = "flex";

  actualizarInfo(``);
  document.getElementById("puntos1").innerHTML = "";
  document.getElementById("puntos2").innerHTML = "";
  ronda = 1;
  contadorJugador1 = 0;
  contadorJugador2 = 0;
}

function juego() {
  var seleccionJugador = document.getElementById("opciones").value;
  var opcionRivalSeleccionada = Math.floor(Math.random() * 3);

  switch (seleccionJugador) {
    case "piedra":
      document.getElementById("imagenjugador1").src = "images/piedra.gif";
      if (opcionRivalSeleccionada == 2) {
        document.getElementById("imagenjugador2").src = "images/tijera.gif";
        contadorJugador1++;
        actualizarInfo(`La ronda ${ronda} fue ganada por ${vNombreGlobal}`);
      } else if (opcionRivalSeleccionada == 1) {
        document.getElementById("imagenjugador2").src = "images/papel.gif";
        contadorJugador2++;
        actualizarInfo(`La ronda ${ronda} fue ganada por Charly`);
      } else {
        document.getElementById("imagenjugador2").src = "images/piedra.gif";
        actualizarInfo(`La ronda ${ronda} termino en empate`);
      }

      break;
    case "papel":
      document.getElementById("imagenjugador1").src = "images/papel.gif";
      if (opcionRivalSeleccionada == 0) {
        document.getElementById("imagenjugador2").src = "images/piedra.gif";
        contadorJugador1++;
        actualizarInfo(`La ronda ${ronda} fue ganada por ${vNombreGlobal}`);
      } else if (opcionRivalSeleccionada == 2) {
        document.getElementById("imagenjugador2").src = "images/tijera.gif";
        contadorJugador2++;
        actualizarInfo(`La ronda ${ronda} fue ganada por Charly`);
      } else {
        document.getElementById("imagenjugador2").src = "images/papel.gif";
        actualizarInfo(`La ronda ${ronda} termino en empate`);
      }
      break;

    case "tijera":
      document.getElementById("imagenjugador1").src = "images/tijera.gif";
      if (opcionRivalSeleccionada == 1) {
        document.getElementById("imagenjugador2").src = "images/papel.gif";
        contadorJugador1++;
        actualizarInfo(`La ronda ${ronda} fue ganada por ${vNombreGlobal}`);
      } else if (opcionRivalSeleccionada == 0) {
        document.getElementById("imagenjugador2").src = "images/piedra.gif";
        contadorJugador2++;
        actualizarInfo(`La ronda ${ronda} fue ganada por Charly`);
      } else {
        document.getElementById("imagenjugador2").src = "images/tijera.gif";
        actualizarInfo(`La ronda ${ronda} termino en empate`);
      }
      break;
  }

  document.getElementById("puntos1").innerHTML = contadorJugador1;
  document.getElementById("puntos2").innerHTML = contadorJugador2;
  document.getElementById("botonreset").style.display = "flex";
  document.getElementById("botonjugar").style.display = "none";

  if (contadorJugador1 == 3) {
    Swal.fire({
      icon: "success",
      title: "¡Ganaste , Felicitaciones!",
      width: 600,
      padding: "3em",
      background:
        "#fff url(https://sweetalert2.github.io/#examplesimages/trees.png)",
      backdrop: `
  rgb(0, 0, 0)
    url("https://media1.tenor.com/images/cf7ab5a05d7457419fd5a801e4464cbb/tenor.gif?itemid=9508230")
    left top
    no-repeat 
    
  `,
    });
    document.getElementById("botonreset").style.display = "none";
    document.getElementById("botonNuevoJuego").style.display = "flex";
    actualizarInfo(`¡Ganaste, Felicitaciones!`);
  } else if (contadorJugador2 == 3) {
    Swal.fire({
      title: "¡Perdiste!",
      text: "Esperamos que tengas más suerte la proxima vez...",
      icon: "error",
      confirmButtonText: "Aceptar",
    });
    document.getElementById("botonreset").style.display = "none";
    document.getElementById("botonNuevoJuego").style.display = "flex";
    actualizarInfo(
      `¡Perdiste!, Esperamos que tengas más suerte la proxima vez...`
    );
  } else {
  }

  ronda++;
  document.getElementById("opciones").style.display = "none";
  document.getElementById("botonreset").innerHTML = `Jugar ronda ${ronda} `;
}
